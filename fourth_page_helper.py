import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

data = {
    'x': [y for y in range(22)],
    'y': [
            873, 801, 796, 803, 726, 680, 650, 597, 467, 523,
            524, 567, 597, 550, 530, 754, 817, 784, 723, 696,
            723, 727
          ]
}

df = pd.DataFrame(data, columns=['x', 'y'])


def get_graph_4(flip_Y, lim_0_max, fill, lines):
    fig = go.Figure()
    mode = 'markers'
    if lines:
        mode = 'lines+markers'
    fill_mod = None
    if fill:
        fill_mod = 'tozeroy'
    fig.add_trace(go.Scatter(
        x=df['x'],
        y=df['y'],
        fill=fill_mod,
        mode=mode,
        line_color='red'
    ))
    if lim_0_max:
        if flip_Y:
            fig.update_yaxes(range=[df['y'].max() * 1.1, 0])
        else:
            fig.update_yaxes(range=[0, df['y'].max()*1.1])
    else:
        if flip_Y:
            fig.update_yaxes(range=[df['y'].max() * 1.1, df['y'].min() * 0.9])
        else:
            fig.update_yaxes(range=[df['y'].min()*0.9, df['y'].max()*1.1])
    fig.update_layout(template='plotly_white')
    return fig