# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
from plotly.colors import sequential

import numpy as np

width = 1
depth = 1
space_between = 1.5
x_pos = (width + space_between)
data = np.array([[64.0, 62.1, 55.6, 52.0, 53.3],
                 [33.3, 38.5, 42.1, 43.0, 44.0],
                 [4.1, 7.3, 15.7, 28.2, 34.3],
                 [13.9, 15.5, 16.2, 16.8, 17.2],
                 [22.0, 23.2, 20.9, 25.4, 27.0]])

years = ['2016', '2017', '2018', '2019', '2020']
companies = ['Apple', 'Samsung', 'Huawei', 'Xiaomi', 'Lenovo']
colors = [[sequential.Inferno[0], sequential.Inferno[2], sequential.Inferno[4]],
          [sequential.Viridis[0], sequential.Viridis[2], sequential.Viridis[4]],
          [sequential.Plasma[0], sequential.Plasma[2], sequential.Plasma[4]],
          [sequential.Plotly3[0], sequential.Plotly3[2], sequential.Plotly3[4]],
          [sequential.Cividis[0], sequential.Cividis[2], sequential.Cividis[4]]]


def layout_3d_update(fig, width=width, scale_to_100=False):
    if scale_to_100:
        zaxis = dict(nticks=4, range=[0, 100], )
    else:
        zaxis = dict(
            nticks=4, ticks='outside',
            tick0=0, tickwidth=4)

    fig.update_layout(scene=dict(
        xaxis=dict(
            ticktext=companies,
            tickvals=[0, x_pos + width / 2, 2 * x_pos + width / 2,
                      3 * x_pos + width / 2, 4 * x_pos + width / 2]),
        yaxis=dict(
            ticktext=years,
            tickvals=[0, x_pos + width / 2, 2 * x_pos + width / 2,
                      3 * x_pos + width / 2, 4 * x_pos + width / 2]),
        zaxis=zaxis, ),
        width=700,
        margin=dict(r=10, l=10, b=10, t=10),
        scene_camera=dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=1.5, y=1.5, z=1.25)
        ),
        showlegend=False)


def get_mesh(pos_x, pos_y, height, colors):
    x = pos_x * (width + space_between)
    y = pos_y * (depth + space_between)
    mesh = go.Mesh3d(
        # 8 vertices of a cube
        x=[x, x, x + width, x + width, x, x, x + width, x + width],
        y=[y, y + depth, y + depth, y, y, y + depth, y + depth, y],
        z=[0, 0, 0, 0, height, height, height, height],
        colorbar_title='z',
        colorscale=[[0, colors[0]],
                    [0.5, colors[1]],
                    [1, colors[2]]],
        # Intensity of each vertex, which will be interpolated and color-coded
        intensity=np.linspace(0, 1, 12, endpoint=True),
        intensitymode='cell',
        # i, j and k give the vertices of triangles
        i=[7, 0, 0, 0, 4, 4, 6, 6, 4, 0, 3, 2],
        j=[3, 4, 1, 2, 5, 6, 5, 2, 0, 1, 6, 3],
        k=[0, 7, 2, 3, 6, 7, 1, 1, 5, 5, 7, 6],
        name='srututu',
        showscale=False,
        showlegend=False
    )
    return mesh


def get_plot1(scale_to_100, show_labels):
    # TODO show_labels doesn't work in this mode
    fig = go.Figure(data=[get_mesh(i, j, data[i][j], colors[i]) for i in range(5) for j in range(5)])
    fig.update_layout(template='plotly_white')
    layout_3d_update(fig, scale_to_100=scale_to_100)
    return fig


def get_plot2(scale_to_100, show_labels):
    fig = go.Figure(data=[go.Bar(name=companies[i], x=years, y=data[i, :], text=data[i, :]) for i in range(5)])
    fig.update_layout(barmode='group', template='plotly_white')
    if scale_to_100:
        fig.update_yaxes(range=[0, 100])
    if show_labels:
        fig.update_traces(texttemplate='%{text:.2s}', textposition='outside')
    return fig


def get_plot3(scale_to_100, show_labels):
    fig = go.Figure()
    # Add traces
    if show_labels:
        for i in range(5):
            fig.add_trace(go.Scatter(x=years, y=data[i, :],
                                     mode='lines+markers+text',
                                     text=data[i, :],
                                     textposition='top center',
                                     name=companies[i]))
    else:
        for i in range(5):
            fig.add_trace(go.Scatter(x=years, y=data[i, :],
                                     mode='lines+markers',
                                     name=companies[i]))
    fig.update_xaxes(dtick=1)
    if scale_to_100:
        fig.update_yaxes(range=[0, 100])
    fig.update_layout(template='plotly_white')
    return fig


def get_plot4(scale_to_100, show_labels):
    if show_labels:
        text = data.flatten()
    else:
        text = None
    fig = px.scatter_3d(y=[i * x_pos + width / 2 for i in range(5)] * 5,
                        x=[i * x_pos for i in range(5) for _ in range(5)],
                        z=data.flatten(),
                        color=[colors[i][0] for i in range(5) for _ in range(5)],
                        text=text)
    fig.update_layout(template='plotly_white')
    layout_3d_update(fig, width=0, scale_to_100=scale_to_100)
    return fig


def get_plot(p1, p2, p3, p4):
    if p1:
        if p2:
            return get_plot3(p3, p4)
        else:
            return get_plot2(p3, p4)
    else:
        if p2:
            return get_plot4(p3, p4)
        else:
            return get_plot1(p3, p4)


html_elements = [
    html.Center(html.H2('Zbędny trzeci wymiar?'), className='title-area'),
    html.Div(
        className='flex-col top-area',
        children=[
            dbc.Col(
                id="first-chart",
                md=7,
                children=dcc.Graph(
                    id='plot1'
                )
            ),
            dbc.Col(
                id='first-description',
                md=5,
                children=html.Div(
                    className='description-area',
                    children=dcc.Markdown('''
                    Wykres jest klasycznym przykładem złego wykresu kolumnowego.  
                    Dane pokazują udział firm na rynku telefonów komórkowych na przestrzeni lat.  
                    Oś Z przedstawia miliardy dolarów przychodu.  
                    Jak najlepiej przedstawić i porównać kilka parametrów zmieniających się na przestrzeni lat?
                '''))
            )
        ]
    ), html.Div(
        className='flex-col bottom-area',
        children=[
            dbc.Col(
                id='first-options',
                md=4,
                children=html.Div(
                    className='options-area',
                    children=[
                        html.H4('Opcje:'),
                        dcc.Checklist(
                            id='checklist1',
                            options=[
                                {'label': 'Połącz wymiary zawierające firmy i lata razem', 'value': '1'},
                                {'label': 'Zamień kolumny na punkty', 'value': '2'},
                                {'label': 'Zeskaluj do 100', 'value': '3'},
                                {'label': 'Dodaj etykiety', 'value': '4'}
                            ],
                            labelStyle={'display': 'block'},
                            value=[]
                        )
                    ])
            ),
            dbc.Col(
                id='results1',
                md=5,
                children=dcc.Markdown(
                    id='md1',
                    children='''''')
            ),
            dbc.Col(
                id='first-submit',
                md=3,
                children=[
                    dbc.Button('Sprawdź poprawność',
                               id='sub1',
                               size='lg',
                               color='primary',
                               className='btn-submit')
                ]
            )
        ]
    )]
