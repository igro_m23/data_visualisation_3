import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
from plotly.colors import sequential

import numpy as np
import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots

df = px.data.iris()
data = df.copy()
data['species2'] = np.where(data['species'] == 'setosa', 'blue',
                            np.where(data['species'] == 'virginica', 'red', 'green'))


# entry
def plot_bubble():
    fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                     size='petal_length')
    fig.update_layout(
        xaxis_title="Sepal width",
        yaxis_title="Sepal length",
        template='plotly_white')
    return fig


# 3d
def plot_3d():
    fig = px.scatter_3d(df, x='sepal_length', y='sepal_width', z='petal_length',
                        color='species',
                        opacity=0.7)
    fig.update_layout(
        scene_xaxis_title="Sepal width",
        scene_yaxis_title="Sepal length",
        scene_zaxis_title="Petal length",
        template='plotly_white')
    return fig


r'''
# labels
fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                 size='petal_length', text='species')
fig.update_traces(textposition='top center')
# fig.show()

# symbols
fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                 size='petal_length', symbol='species')
# fig.show()


fig = px.scatter_matrix(df,
                        dimensions=["sepal_width", "sepal_length", "petal_length"],
                        color="species")


# fig.show(renderer='firefox')
'''


def plot_pair():
    fig = make_subplots(rows=3, cols=3)

    # Add traces
    for row in range(3):
        for col in range(3):
            if row == col:
                fig.add_trace(go.Histogram(x=data.iloc[:, row],
                                           showlegend=False), 
                                           row=row + 1, col=col + 1)
            else:
                for specie, s_data in data.groupby('species'):
                    fig.add_trace(go.Scatter(x=s_data.iloc[:, col], y=s_data.iloc[:, row],
                                             mode='markers',
                                             marker=dict(color=s_data['species2'],opacity=0.4),
                                             name=specie,
                                             showlegend=True if (row == 0 and col == 1) else False), 
                                             row=row + 1,
                                  col=col + 1)

    fig.update_xaxes(title_text="Sepal length", row=3, col=1)
    fig.update_xaxes(title_text="Sepal width", row=3, col=2)
    fig.update_xaxes(title_text="Petal length", row=3, col=3)

    # Update yaxis properties
    fig.update_yaxes(title_text="Sepal length", row=1, col=1)
    fig.update_yaxes(title_text="Sepal width", row=2, col=1)
    fig.update_yaxes(title_text="Petal length", row=3, col=1)
    fig.update_layout(legend_title_text=" ",
                      template='plotly_white')

    return fig


def get_graph_2(opt):
    if opt == 0:
        return plot_bubble()
    elif opt == 1:
        return plot_3d()
    elif opt == 2:
        return plot_pair()
    else:
        raise Exception('Cos sie wychrzanilo')
