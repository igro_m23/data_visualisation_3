import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

data = {
    'label': list("ABCDE"),
    '2010': [0.1, 0.1, 0.2, 0.4, 0.2],
    '2011': [0.15, 0.2, 0.15, 0.3, 0.2],
    '2012': [0.2, 0.2, 0.15, 0.3, 0.15],
    '2013': [0.1, 0.3, 0.2, 0.4, 0.0],
    '2014': [0.1, 0.5, 0.2, 0.15, 0.05]
}

df = pd.DataFrame(data)


def get_graph_3(graph_type):

    labels = df['label']


    if graph_type==0 :
        # Create subplots: use 'domain' type for Pie subplot
        fig = make_subplots(rows=1, 
                            cols=len(df.columns), 
                            specs=[[{'type':'domain'}] * len(df.columns)],
                            subplot_titles=df.columns[1:],
                            )
                            

        for i, c in enumerate(df.columns[1:],1):
            fig.add_trace(go.Pie(labels=labels, 
                                 values=df[c], 
                                 name=c,sort=False),
                    1, i)
    elif graph_type==1 :
        fig = go.Figure(data=[go.Bar(name=df.iloc[i,0], 
                                     x=df['label'], 
                                     y=df[c], 
                                     text=c, 
                                     textposition='outside') for i, c in enumerate(df.columns[1:])])
        # Change the bar mode
        fig.update_layout(barmode='group', showlegend=False, yaxis=dict(range=[0,.55]))

    elif graph_type==2:
        # Create traces
        fig = go.Figure()
        for i, c in enumerate(df.columns[1:],0):
            fig.add_trace(go.Scatter(x=df.columns[1:], y=df.iloc[i][1:],
                                mode='lines+markers',
                                name=df.label[i]))
        fig.update_layout(
            xaxis_type='category'
                #range=[int(x) for x in list(df.columns[1:])]
        )
    elif graph_type==3:
        fig = go.Figure()
        for i, c in enumerate(df.columns[1:],0):
            fig.add_trace(go.Scatter(
                x=df.columns[1:], 
                y=df.iloc[i][1:],
                hoverinfo='x+y',
                mode='lines',
                #line=dict(width=0.5, color='rgb(131, 90, 241)'),
                name=df.label[i],
                stackgroup='one' # define stack group
            ))
        fig.update_layout(
            xaxis_type='category'
                #range=[int(x) for x in list(df.columns[1:])]
        )

    # Use `hole` to create a donut-like pie chart
    #fig.update_traces(hole=.4, hoverinfo="label+percent+name")

    fig.update_layout(title_text="Udział grup w produkcji na przestrzeni lat.", template='plotly_white')
    
    return fig