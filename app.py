# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import visdcc
from dash import dependencies

from fourth_page_helper import get_graph_4
from bad_visu_1 import html_elements as html_elements_page_1, get_plot
from third_page_helper import get_graph_3
from bad_visu_5 import html_elements as html_elements_page_5, get_plot5
from bad_visu_2 import get_graph_2

app = dash.Dash(__name__, external_stylesheets=[
    dbc.themes.BOOTSTRAP
])

server = app.server

app.layout = html.Div(
    children=[
        html.Div(
            id='main',
            className='scroll-container',
            children=[
                html.Section(
                    id='title-section',
                    children=html.Div(
                        className="screen-height",
                        children=[
                            html.Center(html.Div("Błędy wizualizacji danych", id='title-main')),
                            html.Center(html.Div('Sprawdź czy umiesz je wykryć i poprawić', id='subtitle'))
                        ]
                    )
                ),
                html.Section(
                    id='first-section',
                    children=html.Div(
                        className="screen-height",
                        children=html_elements_page_1
                    )
                ),
                html.Section(
                    id='second-section',
                    children=html.Div(
                        className="screen-height flex-row",
                        children=[
                            html.Center(html.H3('Trzy zmienne na wykresie punktowym'), className='title-area'),
                            html.Div(
                                className='flex-col top-area',
                                children=[
                                    dbc.Col(
                                        id="second-chart",
                                        md=7,
                                        children=dcc.Graph(
                                            id='second-graph'
                                        )
                                    ),
                                    dbc.Col(
                                        id='second-description',
                                        md=5,
                                        children=html.Div(
                                            className='description-area',
                                            children=[html.P(
                                                'Prosty wykres punktowy trzech zmiennych.'),
                                                html.P(
                                                    'Naszym celem jest przedstawienie i porównanie 3 zmiennych '
                                                    'jednocześnie - Sepal length, Sepal width oraz Petal length.'),
                                            ])
                                    )
                                ]
                            ),
                            html.Div(
                                className='flex-col bottom-area',
                                children=[
                                    dbc.Col(
                                        id='second-options',
                                        md=4,
                                        children=html.Div([
                                            html.H4('Wybierz rodzaj wykresu:'),
                                            html.Div([
                                                dcc.RadioItems(
                                                    id='second-option-1',
                                                    options=[
                                                        {'value': 0, 'label': ' Bąbelkowy'},
                                                        {'value': 1, 'label': ' 3D'},
                                                        {'value': 2, 'label': ' Pair plot'},
                                                    ],
                                                    labelStyle={'margin-right': '1rem'},
                                                    value=0
                                                )
                                            ])
                                        ])),
                                    dbc.Col(
                                        md=5,
                                        children=dcc.Markdown(
                                            id='md2',
                                            children='''''')
                                    ),
                                    dbc.Col(
                                        id='second-submit',
                                        md=3,
                                        children=[
                                            dbc.Button('Sprawdź poprawność',
                                                       id='sub2',
                                                       size='lg',
                                                       color='primary',
                                                       className='btn-submit')
                                        ]
                                    )
                                ]
                            )
                        ]
                    )
                ), html.Section(
                    id='third-section',
                    children=html.Div(
                        className='screen-height flex-row',
                        children=[
                            html.Center(html.H2('Kołowe szeregi czasowe'), className='title-area'),
                            html.Div(
                                className='flex-col top-area',
                                children=[
                                    dbc.Col(
                                        id="third-chart",
                                        md=7,
                                        children=dcc.Graph(
                                            id='third-graph'
                                        )
                                    ),
                                    dbc.Col(
                                        id='third-description',
                                        md=5,
                                        children=html.Div(
                                            className='description-area',
                                            children=[html.P(
                                                'Wykres ten przedstawia procentowy udział grup w produkcji towarów według pewnych działów, na przestrzeni lat.'),
                                                html.P(
                                                    'Celem wizualizacji jest porównanie zmian udziału między grupami w czasie.'),
                                                html.P(
                                                    'Przykładowe pytania które można sobie w tym celu zadać, to: Jak różni się udział procentowy grup w 2012 oraz 2014 roku? Czy wzrost udziału grupy B jest powiązany ze spadkiem udziału grupy D?'),
                                                html.P(children=html.Div(html.A('Problem zainspirowany GUS',
                                                                                href='https://stat.gov.pl/portal-edukacyjny/info-dla-studentow/zakladka-1/#powierzchniowe')))
                                            ])
                                    )
                                ]
                            ),
                            html.Div(
                                className='flex-col bottom-area',
                                children=[
                                    dbc.Col(
                                        id='third-options',
                                        md=4,
                                        children=html.Div(
                                            className='options-area',
                                            children=[
                                                html.H4('Wybierz rodzaj wykresu:'),
                                                html.Div([
                                                    dcc.RadioItems(
                                                        id='third-option-1',
                                                        options=[
                                                            {'value': 0, 'label': 'Kołowy'},
                                                            {'value': 1, 'label': 'Słupkowy'},
                                                            {'value': 2, 'label': 'Liniowy'},
                                                            {'value': 3, 'label': 'Liniowo-warstwowy'},
                                                        ],
                                                        labelStyle={'margin-right': '1rem'},
                                                        value=0
                                                    )
                                                ]),
                                            ])
                                    ),
                                    dbc.Col(
                                        md=5,
                                        children=dcc.Markdown(
                                            id='md3',
                                            children='''''')
                                    ),
                                    dbc.Col(
                                        id='third-submit',
                                        md=3,
                                        children=[
                                            dbc.Button('Sprawdź poprawność',
                                                       id='sub3',
                                                       size='lg',
                                                       color='primary',
                                                       className='btn-submit')
                                        ]
                                    )
                                ]
                            )

                        ]
                    )
                ),
                html.Section(
                    id='fourth-section',
                    children=html.Div(
                        className='screen-height flex-row',
                        children=[
                            html.Center(html.H2('Do góry nogami'), className='title-area'),
                            html.Div(
                                className='flex-col top-area',
                                children=[
                                    dbc.Col(
                                        id="fourth-chart",
                                        md=7,
                                        children=dcc.Graph(
                                            id='fourth-graph'
                                        )
                                    ),
                                    dbc.Col(
                                        id='fourth-description',
                                        md=5,
                                        children=html.Div(
                                            className='description-area',
                                            children=dcc.Markdown(
                                                '''
                                                Wykres jest inspirowany [realnym przykładem](https://www.businessinsider.in/This-Chart-Shows-What-Happened-To-Gun-Deaths-In-Florida-After-Stand-Your-Ground-Was-Enacted/articleshow/30635752.cms).
                                                
                                                Wizualizacja przedstawiała ilość zabójstw z wykorzystaniem bronii palnej na Florydzie w latach 1990-2012. 
                                                **Odwrócenie osi Y** miało symbolicznie przedstawiać spływającą krew (czerwone wypałnienie).
                                                Efektem ubocznym jest złamanie powszechnie przyjętej konwencji co sprawia, że wykres może być mylący. 
                                                
                                                Twoim zadaniem jest poprawienie wykresu.
                                                '''
                                            )
                                        )
                                    )
                                ]
                            ),
                            html.Div(
                                className='flex-col bottom-area',
                                children=[
                                    dbc.Col(
                                        id='fourth-options',
                                        md=4,
                                        children=html.Div(
                                            className='options-area',
                                            children=[
                                                html.H4('Opcje:'),
                                                html.Div([
                                                    html.H6('Zwrot osi Y:'),
                                                    dcc.RadioItems(
                                                        id='fourth-option-1',
                                                        options=[
                                                            {'value': 0, 'label': 'Do góry'},
                                                            {'value': 1, 'label': 'Do dołu'},
                                                        ],
                                                        labelStyle={'margin-right': '1rem'},
                                                        value=1
                                                    )
                                                ]),
                                                html.Div([
                                                    html.H6('Limity osi Y:'),
                                                    dcc.RadioItems(
                                                        id='fourth-option-2',
                                                        options=[
                                                            {'value': 0, 'label': '[min, max]'},
                                                            {'value': 1, 'label': '[0, max]'},
                                                        ],
                                                        labelStyle={'margin-right': '1rem'},
                                                        value=0
                                                    )
                                                ]),
                                                html.Div([
                                                    html.H6(
                                                        'Wypełnienie przestrzeni pomiędzy osią Y a wykresem kolorem:'),
                                                    dcc.RadioItems(
                                                        id='fourth-option-3',
                                                        options=[
                                                            {'value': 0, 'label': 'Nie'},
                                                            {'value': 1, 'label': 'Tak'},
                                                        ],
                                                        labelStyle={'margin-right': '1rem'},
                                                        value=1
                                                    )
                                                ]),
                                                html.Div([
                                                    html.H6("Linie łączące punkty wykresu:"),
                                                    dcc.RadioItems(
                                                        id='fourth-option-4',
                                                        options=[
                                                            {'value': 0, 'label': 'Nie'},
                                                            {'value': 1, 'label': 'Tak'},
                                                        ],
                                                        labelStyle={'margin-right': '1rem'},
                                                        value=0
                                                    )
                                                ])

                                            ])
                                    ),
                                    dbc.Col(
                                        id='results4',
                                        md=5,
                                        children=dcc.Markdown(
                                            id='md4',
                                            children='''''')

                                    ),
                                    dbc.Col(
                                        id='fourth-submit',
                                        md=3,
                                        children=[
                                            dbc.Button('Sprawdź poprawność',
                                                       id='sub4',
                                                       size='lg',
                                                       color='primary',
                                                       className='btn-submit')
                                        ]
                                    )
                                ]
                            )

                        ]
                    )
                ),
                html.Section(
                    id='fifth-section',
                    children=html.Div(
                        className="screen-height",
                        children=html_elements_page_5
                    )
                )
            ]),
        visdcc.Run_js(id='javascript',
                      run='''
                            new fullScroll({	
                                mainElement: 'main', 
                                sections:['title-section', 'map-section','prediction-section'],
                                displayDots: true,
                                dotsPosition: 'right',
                                animateTime: 0.7,
                                animateFunction: 'ease'	
                            });
                            '''
                      ),
    ]
)


@app.callback(
    dependencies.Output('fourth-graph', 'figure'),
    [
        dependencies.Input('fourth-option-1', 'value'),
        dependencies.Input('fourth-option-2', 'value'),
        dependencies.Input('fourth-option-3', 'value'),
        dependencies.Input('fourth-option-4', 'value')
    ]
)
def generate_graph_4(opt_1, opt_2, opt_3, opt_4):
    return get_graph_4(
        opt_1 == 1,
        opt_2 == 1,
        opt_3 == 1,
        opt_4 == 1)


@app.callback(
    dependencies.Output("md4", "children"),
    [
        dependencies.Input('fourth-option-1', 'value'),
        dependencies.Input('fourth-option-2', 'value'),
        dependencies.Input('fourth-option-3', 'value'),
        dependencies.Input('fourth-option-4', 'value'),
        dependencies.Input("sub4", "n_clicks")
    ]
)
def update_result4(opt_1, opt_2, opt_3, opt_4, n):
    if n is None or n % 2 == 0:
        return ''''''
    if opt_1 == 0 and \
            opt_2 == 1 and \
            opt_3 == 0 and \
            opt_4 == 1:
        return '''
        ![dobrze](https://user-images.githubusercontent.com/43205483/84391542-69e45700-abf9-11ea-8612-811f35935105.png)  
        
        Oś Y powinna być zwrócona **do góry**. 
         
        Limity osi Y powinny być ustalone na **0, max**. Pozwala to na porównanie zmian do skali całego zjawiska.
        
        Wypełnienie przestrzeni pomiędzy wykresem, a osią Y jest **zbyteczne**.
        
        Linie łączące punkty sprawiają, że wykres jest **bardziej czytelny**. 
        '''
    else:
        return '''
         ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)  
        
        Oś Y powinna być zwrócona **do góry**. 
         
        Limity osi Y powinny być ustalone na **0, max**. Pozwala to na porównanie zmian do skali całego zjawiska.
        
        Wypełnienie przestrzeni pomiędzy wykresem, a osią Y jest **zbyteczne**.
        
        Linie łączące punkty sprawiają, że wykres jest **bardziej czytelny**.  
        '''


@app.callback(
    dependencies.Output('third-graph', 'figure'),
    [
        dependencies.Input('third-option-1', 'value'),
    ]
)
def generate_graph_3(opt_1):
    return get_graph_3(opt_1)


@app.callback(
    dependencies.Output('second-graph', 'figure'),
    [
        dependencies.Input('second-option-1', 'value'),
    ]
)
def generate_graph_2(opt_1):
    return get_graph_2(opt_1)


@app.callback(
    dependencies.Output("md2", "children"),
    [
        dependencies.Input("sub2", "n_clicks"),
        dependencies.Input('second-option-1', 'value'),
    ],
)
def update_result2(n, value):
    if n is None or n % 2 == 0:
        return ''''''

    if value == 0:
        return '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)
            
            Trzecia zmienna jest przedstawiona jako wielkość bąbelka, co zdecydowanie utrudnia porównywanie.
            '''
    elif value == 1:
        return '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)

            Wykres 3d mógłby ukazać ciekawe zależności, ale w tym przypadku jest zbyt duży chaos i niewiele widać.
            '''

    elif value == 2:
        return '''
            ![dobrze](https://user-images.githubusercontent.com/43205483/84391542-69e45700-abf9-11ea-8612-811f35935105.png)

            Pair plot jest najlepszym wyborem ze względu na swoja czytelność. Pozwala wygodnie 
            i precyzyjnie porównać pary zmiennych.
            Dodaktowo dostajemy jeszcze histogram dla każdej zmiennej.
        '''
    return score


@app.callback(
    dependencies.Output("md3", "children"),
    [
        dependencies.Input("sub3", "n_clicks"),
        dependencies.Input('third-option-1', 'value'),
    ],
)
def update_result3(n, value):
    if n is None or n % 2 == 0:
        return ''''''

    if value == 0:
        return '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)
            
            Wykresy kołowe nie spawdzają się w przypadku prezentacji danych zmieniających się w czasie.
        
            Zauważ jak trudno jest określić zmiany w grupie E w latach 2010-2014 na tym wykresie.
            '''
    elif value == 1:
        return '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)
            
            Wykres słupkowy nie pozwala zobaczyć na pierwszy rzut oka udziału procentowego grup w jednym roku.
             
            Zobacz jak trudno jest porównać udział grup w 2013 roku. 
            '''
    elif value == 2:
        return '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)
            
            Wykres liniowy nie pozwala zobaczyć na pierwszy rzut oka udziału procentowego grup w jednym roku.

            Ponadto w związku z podobnymi udziałami wielu grup, jest on nieczytelny w latach 2010-2012.
        '''
    elif value == 3:
        return '''
            ![dobrze](https://user-images.githubusercontent.com/43205483/84391542-69e45700-abf9-11ea-8612-811f35935105.png)
            
            Wykres liniowo-warstwowy jest najlepszym wyborem dla tych danych.
                
            Na pierwszy rzut oka widać zarówno trendy (wzrost znaczenia B w ostatnich latach, spadek D),
            jak i podział procentowy w danym roku.
        '''
    return score


@app.callback(
    dependencies.Output("md1", "children"),
    [
        dependencies.Input("checklist1", "value"),
        dependencies.Input("sub1", "n_clicks")
    ]
)
def update_result4(values, n):
    if n is None or n % 2 == 0:
        return ''''''
    score = '''
          
    '''
    if set(values) == {'1', '2', '4'}:
        score = '![dobrze](https://user-images.githubusercontent.com/43205483/84391542-69e45700-abf9-11ea-8612-811f35935105.png)  '
    else:
        score = '![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)  '

    score += '''
        Nie ma powodu, by wykres ten był w **3D**.  
        ​  
        **Punkty** zamiast kolumn pozwalają na zmniejszenie szerokości wykresu i poprawiają data-ink ratio.  
        ​  
        Zwiększenie skali zmniejsza procent obrazu, na którym znajdują się wartościowe informacje.  
        ​  
        Dodanie **etykiet** poprawia czytelność.
        '''
    return score


@app.callback(
    dependencies.Output("plot1", "figure"),
    [dependencies.Input("checklist1", "value")],
)
def update_plot1(values):
    p = [False, False, False, False]
    for value in values:
        p[int(value) - 1] = True
    return get_plot(p[0], p[1], p[2], p[3])


@app.callback(
    dependencies.Output("plot5", "figure"),
    [dependencies.Input("checklist5", "value")],
)
def update_plot5(values):
    p = [False, False, False, False]
    for value in values:
        p[int(value) - 1] = True
    return get_plot5(p[0], p[1], p[2], p[3])


@app.callback(
    dependencies.Output("md5", "children"),
    [dependencies.Input("sub5", "n_clicks"), dependencies.Input("checklist5", "value")],
)
def update_result5(n, values):
    p = [False, False, False, False]
    for value in values:
        p[int(value) - 1] = True
    if n is None or n % 2 == 0:
        return ''''''
    if p[3] and not p[2]:
        if p[1]:
            if p[0]:
                return (
                    '''
                    ![?](https://user-images.githubusercontent.com/43205483/84391709-a1eb9a00-abf9-11ea-8961-4390cddf0064.png)  
                    &nbsp;  
                    Wykorzystanie **wykresów słupkowych** i **wartości bezwzględnych** sprawia, że wykres jest bardziej czytelny.  
                    &nbsp;  
                    Zastosowanie **kolorów aplikacji** może i wygląda estetycznie, ale sprawia, że nazwy poszczególnych słupków (Total/Active users)
                    są widoczne dopiero w tooltipie i pogarsza czytelność wykresu.  
                    &nbsp;  
                    **Połączenie** w skumulowany wykres słupkowy nie jest złym pomysłem, ale wydaje się, że w formie oddzielnych słupków wykres
                    jest prostszy i przez to bardziej przejrzysty.
                    '''
                )
            return (
                '''
                ![dobrze](https://user-images.githubusercontent.com/43205483/84391542-69e45700-abf9-11ea-8612-811f35935105.png)  
                &nbsp;  
                Wykorzystanie **wykresów słupkowych** i **wartości bezwzględnych** sprawia, że wykres jest bardziej czytelny.  
                &nbsp;  
                Zastosowanie **kolorów aplikacji** może i wygląda estetycznie, ale sprawia, że nazwy poszczególnych słupków (Total/Active users)
                są widoczne dopiero w tooltipie i pogarsza czytelność wykresu.  
                &nbsp;  
                **Połączenie** w skumulowany wykres słupkowy nie jest złym pomysłem, ale wydaje się, że w obecnej formie wykres jest prostszy
                i przez to bardziej przejrzysty.
                '''
            )
        elif p[0]:
            return (
                '''
                ![?](https://user-images.githubusercontent.com/43205483/84391709-a1eb9a00-abf9-11ea-8961-4390cddf0064.png)  
                &nbsp;  
                Wykorzystanie **wartości bezwzględnych** sprawia, że wykres jest bardziej czytelny.  
                &nbsp;  
                W przypadku wykresów kołowych **połączenie** jest konieczne,  
                aby całkowicie pozbyć się nieintuicyjnych wartości procentowych.  
                &nbsp;  
                Zastosowanie **kolorów aplikacji** może i wygląda estetycznie, ale pogarsza czytelność wykresu.  
                Co prawda na tym wykresie części odpowiadające wartościom Total/Active users są podpisane,  
                jednak mimo to w obecnej formie wykres jest bardziej przejrzysty.  
                &nbsp;  
                Zamiana na **wykres słupkowy** ułatwiłaby odczytanie i porównanie poszczególnych wartości.
                '''
            )

    elif p[1]:
        return (
            '''
            ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)  
            &nbsp;  
            Wykorzystanie **wykresów słupkowych** i **wartości bezwzględnych** sprawia, że wykres jest bardziej czytelny.  
            &nbsp;  
            Zastosowanie **kolorów aplikacji** może i wygląda estetycznie, ale sprawia, że nazwy poszczególnych słupków (Total/Active users)
            są widoczne dopiero w tooltipie i pogarsza czytelność wykresu.  
            &nbsp;  
            **Połączenie** w skumulowany wykres słupkowy nie jest złym pomysłem, ale wydaje się, że w formie oddzielnych słupków wykres
            jest prostszy i przez to bardziej przejrzysty.
            '''
        )
    return (
        '''
        ![źle](https://user-images.githubusercontent.com/43205483/84391545-6bae1a80-abf9-11ea-88d3-4233dd35e535.png)  
        &nbsp;  
        Wykorzystanie **wartości bezwzględnych** sprawia, że wykres jest bardziej czytelny.  
        &nbsp;  
        W przypadku wykresów kołowych **połączenie** jest konieczne,  
        aby całkowicie pozbyć się nieintuicyjnych wartości procentowych.  
        &nbsp;  
        Zastosowanie **kolorów aplikacji** może i wygląda estetycznie, ale pogarsza czytelność wykresu.  
        Nawet jeśli części odpowiadające wartościom Total/Active users są podpisane,  
        roznióżnienie tych wartości kolorem czyni wykres bardziej przejrzystym.  
        &nbsp;  
        Zamiana na **wykres słupkowy** ułatwiłaby odczytanie i porównanie poszczególnych wartości.
        '''
    )

app.title = 'Visu Quiz'
if __name__ == '__main__':
    app.run_server()
