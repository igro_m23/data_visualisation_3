import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
from plotly.subplots import make_subplots

import numpy as np

color_o = 'whitesmoke'
colors1 = ['royalblue', 'limegreen']
names = ['Youtube', 'Facebook', 'Twitter', 'Pinterest', 'LinkedIn']
labels1 = ['Total users', 'Other']
labels2 = ['Active users', 'Other']
colors2 = [['red', 'royalblue', 'deepskyblue', 'crimson', 'steelblue'],
           ['tomato', 'cornflowerblue', 'lightskyblue', 'indianred', 'lightblue']]

data = np.array([[.9948, .3014], [.7093, .6607], [.241, .1966], [.1764, .1862], [.0595, .1076]])
data_abs = data * 5e9
data_abs[:, 1] *= data[:, 0]

d = {'type': 'domain'}


def get_plot_hole(cl, ab):  # plot_hole :) almost as good as go.Figure
    fig = make_subplots(rows=2, cols=5, specs=[[d, d, d, d, d], [d, d, d, d, d]])
    c = colors2 if cl else np.array([colors1] * 5).T
    if ab:
        for i in range(5):
            fig.add_trace(
                go.Pie(labels=labels1, values=[data_abs[i][0], 5e9 - data_abs[i][0]], name=names[i], sort=False,
                       marker_colors=[c[0][i], color_o]), 1, i + 1)
            fig.add_trace(
                go.Pie(labels=labels2, values=[data_abs[i][1], data_abs[i][0] - data_abs[i][1]], name=names[i],
                       sort=False, marker_colors=[c[1][i], color_o]), 2, i + 1)
        fig.update_traces(hole=.8, hoverinfo="label+percent+name+value")
    else:
        for i in range(5):
            fig.add_trace(go.Pie(labels=labels1, values=[data[i][0], 1 - data[i][0]], name=names[i], sort=False,
                                 marker_colors=[c[0][i], color_o]), 1, i + 1)
            fig.add_trace(go.Pie(labels=labels2, values=[data[i][1], 1 - data[i][1]], name=names[i], sort=False,
                                 marker_colors=[c[1][i], color_o]), 2, i + 1)
        fig.update_traces(hole=.8, hoverinfo="label+percent+name")
    fig.update_layout(
        showlegend=False,
        title_text="Total/Active users comparison",
        annotations=[dict(text=names[0], x=0.04, y=0.5, font_size=18, showarrow=False),
                     dict(text=names[1], x=0.24, y=0.5, font_size=18, showarrow=False),
                     dict(text=names[2], x=0.50, y=0.5, font_size=18, showarrow=False),
                     dict(text=names[3], x=0.75, y=0.5, font_size=18, showarrow=False),
                     dict(text=names[4], x=0.96, y=0.5, font_size=18, showarrow=False)])
    return fig


def get_plot_sun(cl, ab):
    fig = make_subplots(rows=1, cols=5, specs=[[d, d, d, d, d]])
    c = colors2 if cl else np.array([colors1] * 5).T
    if ab:
        for i in range(5):
            fig.add_trace(go.Sunburst(
                labels=[names[i], labels1[0], labels2[0]],
                parents=['', names[i], labels1[0]],
                values=[5e9, data_abs[i][0], data_abs[i][1]],
                branchvalues='total',
                marker=dict(colors=['white', c[0][i], c[1][i]]),
                name=names[i]), 1, i + 1)
    else:
        for i in range(5):
            fig.add_trace(go.Sunburst(
                labels=[names[i], labels1[0], labels2[0]],
                parents=['', names[i], labels1[0]],
                values=[1, data[i][0], data[i][0] * data[i][1]],
                branchvalues='total',
                marker=dict(colors=['white', c[0][i], c[1][i]]),
                name=names[i]), 1, i + 1)
    fig.update_traces(hoverinfo="label+name+value")
    fig.update_layout(title_text="Total/Active users comparison")
    return fig


def get_plot_bar(cb, cl, ab):
    c = colors2 if cl else np.array([colors1] * 5).T
    if cb:
        if ab:
            fig = go.Figure(data=[
                go.Bar(name=labels2[0], x=names, y=data_abs[:, 1], marker_color=c[1]),
                go.Bar(name=labels1[0], x=names, y=data_abs[:, 0] - data_abs[:, 1], marker_color=c[0])
            ])
        else:
            fig = go.Figure(data=[
                go.Bar(name=labels2[0], x=names, y=data[:, 1] * data[:, 0], marker_color=c[1]),
                go.Bar(name=labels1[0], x=names, y=data[:, 0] - data[:, 1] * data[:, 0], marker_color=c[0])
            ])
        fig.update_layout(barmode='stack', template='plotly_white', title_text='Total/Active users comparison')
    else:
        if ab:
            fig = go.Figure(data=[
                go.Bar(name=labels1[0], x=names, y=data_abs[:, 0], marker_color=c[0]),
                go.Bar(name=labels2[0], x=names, y=data_abs[:, 1], marker_color=c[1])
            ])
        else:
            fig = go.Figure(data=[
                go.Bar(name=labels1[0], x=names, y=data[:, 0], marker_color=c[0]),
                go.Bar(name=labels2[0], x=names, y=data[:, 1], marker_color=c[1])
            ])
        fig.update_layout(barmode='group', template='plotly_white', title_text='Total/Active users comparison')
    if not ab:
        fig.update_yaxes(ticktext=['20%', '40%', '60%', '80%', '100%'], tickvals=[.2, .4, .6, .8, 1])
    if cl:
        fig.update_layout(showlegend=False)

    return fig


def get_plot5(p1, p2, p3, p4):
    return get_plot_bar(p1, p3, p4) if p2 else get_plot_sun(p3, p4) if p1 else get_plot_hole(p3, p4)


html_elements = [
    html.Center(html.H2('Porównanie wszystkich/aktywnych użytkowników aplikacji'), className='title-area'),
    html.Div(
        className='flex-col top-area',
        children=[
            dbc.Col(
                id="fifth-chart",
                md=7,
                children=dcc.Graph(
                    id='plot5'
                )
            ),
            dbc.Col(
                id='fifth-description',
                md=5,
                children=html.Div(
                    className='description-area',
                    children=[
                        html.P([
                            'Wykres pochodzi z ',
                            html.A('infografiki',
                                   href='https://visual.ly/community/Infographics/business/state-social-media-marketing-2015'),
                            ' dotyczącej mediów społecznościowych. ',
                            'Porównuje on wybrane aplikacje na Androida pod względem liczby wszystkich i aktywnych użytkowników.'
                        ]),
                        html.P('''W oryginalnej formie (z odznaczonymi opcjami) liczba wszystkich użytkowników jest przedstawiona jako procent wszystkich użytkowników Androida, 
                            a liczba aktywnych użytkowników - jako procent użytkowników danej aplikacji.''')
                    ])

            )
        ]
    ), html.Div(
        className='flex-col bottom-area',
        children=[
            dbc.Col(
                id='fifth-options',
                md=4,
                children=html.Div(
                    className='options-area',
                    children=[
                    html.H4('Opcje:'),
                    dcc.Checklist(
                        id='checklist5',
                        options=[
                            {'label': ' Połączenie wykresów', 'value': '1'},
                            {'label': ' Wykres słupkowy', 'value': '2'},
                            {'label': ' Kolory aplikacji', 'value': '3'},
                            {'label': ' Wartości bezwzględne', 'value': '4'}
                        ],
                        labelStyle={'display': 'block'},
                        value=[]
                    )
                ])
            ),
            dbc.Col(
                id='results5',
                md=5,
                children=dcc.Markdown(
                    id='md5',
                    children='''''')
            ),
            dbc.Col(
                id='fifth-submit',
                md=3,
                children=[
                    dbc.Button('Sprawdź poprawność',
                               id='sub5',
                               size='lg',
                               color='primary',
                               className='btn-submit')
                ]
            ),

        ]
    )]
